﻿using System;
using System.Collections.Generic;

namespace ManulPlugin.scr.componentapi
{
    interface DuplicateFinderUi
    {
        event Func<IList<string>> onGetDuplicatesRequest;

        event Action onClearFormattingRequest;

        event Action<string> onCertainDuplicateHighlightRequest;

        event Action<string> onExperimentalDuplicatesRequest;
    }
}
