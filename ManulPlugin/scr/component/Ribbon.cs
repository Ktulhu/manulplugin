﻿using ManulPlugin.scr.component.duplicatefinder;
using Microsoft.Office.Tools.Ribbon;

namespace ManulPlugin
{
    public partial class ManulPlugin
    {
        private DuplicateFinderView duplicateFinderView;

        private void Ribbon_Load(object sender, RibbonUIEventArgs e)
        {
            duplicateFinderView = new DuplicateFinderView();
        }

        private void btnDuplicateFinder_Click(object sender, RibbonControlEventArgs e)
        {
            duplicateFinderView.toggleView();
        }
    }
}
