﻿using System.Collections.Generic;
using ManulPlugin.scr.componentapi;
using Microsoft.Office.Tools;
using System.Windows.Forms;
using ManulPlugin.scr.source;

namespace ManulPlugin.scr.component.duplicatefinder
{
    class DuplicateFinderView
    {
        private readonly IDictionary<int, CustomTaskPane> taskPanesMapping =
            new Dictionary<int, CustomTaskPane>();

        public void toggleView()
        {
            var hwnd = Globals.ThisAddIn.Application.Hwnd;

            CustomTaskPane duplicateFinderPane;
            if (!taskPanesMapping.TryGetValue(hwnd, out duplicateFinderPane))
                duplicateFinderPane = CreateNewPane(hwnd);

            duplicateFinderPane.Visible = !duplicateFinderPane.Visible;
        }

        private CustomTaskPane CreateNewPane(int hwnd)
        {
            var control = new DuplicateFinderControl();
            var model = new DuplicateFinderModel();
            
            control.onGetDuplicatesRequest += model.
            control.onClearFormattingRequest +=
            control.onCertainDuplicateHighlightRequest +=
            control.onExperimentalDuplicatesRequest +=
            Globals.ThisAddIn.CustomTaskPanes.Add(control, "Duplicate Finder");
        }
    }
}
