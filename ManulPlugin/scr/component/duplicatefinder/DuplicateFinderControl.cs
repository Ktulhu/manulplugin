﻿using ManulPlugin.scr.componentapi;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ManulPlugin.scr.component
{
    public partial class DuplicateFinderControl : UserControl, DuplicateFinderUi
    {
        public event Func<IList<string>> onGetDuplicatesRequest;
        public event Action onClearFormattingRequest;
        public event Action<string> onCertainDuplicateHighlightRequest;
        public event Action<string> onExperimentalDuplicatesRequest;
        public DuplicateFinderControl()
        {
            InitializeComponent();
        }

        private void DuplicateFinderControl_Load(object sender, EventArgs e)
        {

        }
    }
}
