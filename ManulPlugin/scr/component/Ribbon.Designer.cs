﻿namespace ManulPlugin
{
    partial class ManulPlugin : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ManulPlugin()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ManulTab = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnDuplicateFinder = this.Factory.CreateRibbonToggleButton();
            this.ManulTab.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ManulTab
            // 
            this.ManulTab.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.ManulTab.Groups.Add(this.group1);
            this.ManulTab.Label = "ManulPlugin";
            this.ManulTab.Name = "ManulTab";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnDuplicateFinder);
            this.group1.Label = "Match Functional";
            this.group1.Name = "group1";
            // 
            // btnDuplicateFinder
            // 
            this.btnDuplicateFinder.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnDuplicateFinder.Label = "Dupplicate Finder";
            this.btnDuplicateFinder.Name = "btnDuplicateFinder";
            this.btnDuplicateFinder.ShowImage = true;
            this.btnDuplicateFinder.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnDuplicateFinder_Click);
            // 
            // ManulPlugin
            // 
            this.Name = "ManulPlugin";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.ManulTab);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_Load);
            this.ManulTab.ResumeLayout(false);
            this.ManulTab.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab ManulTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnDuplicateFinder;
    }

    partial class ThisRibbonCollection
    {
        internal ManulPlugin Ribbon
        {
            get { return this.GetRibbon<ManulPlugin>(); }
        }
    }
}
